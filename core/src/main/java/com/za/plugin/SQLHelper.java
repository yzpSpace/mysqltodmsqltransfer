package com.za.plugin;

import com.za.plugin.transfer.form.*;
import com.za.plugin.transfer.func.*;

import java.util.ArrayList;
import java.util.List;

public class SQLHelper {
    public static void main(String[] args) {
        System.out.println(processSql("select declareType                  as declareType,\n" +
                "       countyId                     as countyId,\n" +
                "       countyName                   as countyName,\n" +
                "       checkYear                    as checkYear,\n" +
                "       ifnull(sum(farmCount), 0)    as farmCount,\n" +
                "       ifnull(sum(projectCount), 0) as projectCount,\n" +
                "       ifnull(sum(landCount), 0)    as landCount,\n" +
                "       CASE\n" +
                "           WHEN count(*) = count(if(approveStatus = 3, 1, NULL)) THEN 'success'\n" +
                "           WHEN count(*) = count(if(approveStatus = 4, 1, NULL)) THEN 'refuse'\n" +
                "           ELSE 'doing' END         as status,\n" +
                "       IF(count(*) = count(if(approveStatus = 3, 1, NULL)) + count(if(approveStatus = 2, 1, NULL)), 1,\n" +
                "          0)                        as display\n" +
                "\n" +
                "from (select t1.id                               as id,\n" +
                "             '设施菜田基地申报'                  as declareType,\n" +
                "             t1.county_name                      as countyName,\n" +
                "             t1.county_id                        as countyId,\n" +
                "             t1.check_year                       as checkYear,\n" +
                "             t1.farm_count                       as farmCount,\n" +
                "             t1.project_count                    as projectCount,\n" +
                "             t1.declare_status                   as declareStatus,\n" +
                "             t1.approve_status                   as approveStatus,\n" +
                "             sum(ifnull(t2.total_land_count, 0)) as landCount\n" +
                "      from frp_produce.tbl_facility_vegetable_task t1\n" +
                "               left join frp_produce.tbl_facility_vegetable_task_item t2 on t2.task_id = t1.id\n" +
                "      where t1.declare_status != 2\n" +
                "        and t1.check_year = 2023\n" +
                "      group by t1.id) T\n" +
                "group by countyId, checkYear\n" +
                "\n" +
                "limit 10;\n" ));
    }
    public static String processSql(String sql) {
        List<FunctionTransfer> functionTransferList = initFunctionTransfer();
        List<FormTransfer> formTransferList = initFormTransfer();
        sql = initTrans(sql);

        for (FormTransfer formTransfer : formTransferList) {
            sql = cleanSql(sql);
            if (formTransfer.isSupport(sql)) {
                sql = formTransfer.transfer(sql);
            }
        }
        for (FunctionTransfer functionTransfer : functionTransferList) {
            sql = cleanSql(sql);
            if (functionTransfer.isSupport(sql)) {
                sql = functionTransfer.transfer(sql);
            }
        }
        return cleanSql(sql);
    }

    // SQLUtils#toMySqlString 会转部分sql为大写
    private static String cleanSql(String sql) {
        return sql.toLowerCase().replaceAll("\n", " ").replaceAll("\t", " ").trim();
    }

    // 达梦的 ' 是字符串，" 是列名,先把所有的'转为",再在selectItem部分转'为"
    private static String initTrans(String sql) {
        sql = sql.replace("\n", " ").
                replace("\t", " ").replaceAll("\\s+", " ");
        // #{ id } => #{id}
        sql = sql.replaceAll("#\\s*\\{\\s*", "#{").replaceAll("\\s+}", "}");
        // 去掉 `` 符号
        sql = sql.replace("`", "").replace("\"", "'").toLowerCase();
        return sql.trim();
    }


    private static List<FormTransfer> initFormTransfer() {
        List<FormTransfer> transferList = new ArrayList<>();
        transferList.add(new GroupByAliasFormTransfer());
        transferList.add(new LikeFormTransfer());
        transferList.add(new IfFormTransfer());
        transferList.add(new IfAliasFormTransfer());
        transferList.add(new SelectFormTransfer());
        transferList.add(new SkipGroupBySortFormTransfer());

        transferList.add(new JoinFormTransfer());
        transferList.add(new CaseWhenFormTransfer());

        transferList.add(new TruncateFormTransfer());
        transferList.add(new OrderByIsNullFormTransfer());
        transferList.add(new ChangeDivFormTransfer());
        transferList.add(new EliminateKeywordAlias());
        transferList.add(new SelectHintFormTransfer());

        return transferList;
    }

    private static List<FunctionTransfer> initFunctionTransfer() {
        List<FunctionTransfer> transferList = new ArrayList<>();
        transferList.add(new ConcatFunctionTransfer());
        transferList.add(new StrToDateFunctionTransfer());
        transferList.add(new DateFormatFunctionTransfer());
        transferList.add(new ConvertFunctionTransfer());
        transferList.add(new CountFunctionTransfer());
        transferList.add(new SubdateFunctionTransfer());
        transferList.add(new DateAddFunctionTransfer());
        transferList.add(new DatediffFunctionTransfer());
        transferList.add(new DateFunctionTransfer());
        transferList.add(new DateSubFunctionTransfer());
        transferList.add(new FormatFunctionTransfer());

        transferList.add(new GroupConcatFunctionTransfer());
        transferList.add(new PowFunctionTransfer());
        transferList.add(new RegexpFunctionTransfer());
        transferList.add(new UnixTimeFunctionTransfer());
        transferList.add(new JsonArrayAggFunctionTransfer());
        transferList.add(new FindInSetFormTransfer());


        return transferList;
    }
}
