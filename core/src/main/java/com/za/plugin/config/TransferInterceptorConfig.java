package com.za.plugin.config;//package com.za.plugin.config;
//
//import com.za.plugin.TransInterceptor;
//import com.za.plugin.util.DataSourceUtil;
//import org.apache.ibatis.plugin.Interceptor;
//import org.apache.ibatis.session.Configuration;
//import org.apache.ibatis.session.SqlSessionFactory;
//import org.springframework.boot.autoconfigure.AutoConfigureAfter;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
//import org.springframework.context.annotation.Lazy;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.PostConstruct;
//import javax.annotation.Resource;
//import java.util.List;
//
//@Component
//@Lazy(value = true)
//
//public class TransferInterceptorConfig   {
//    @Resource
//    private List<SqlSessionFactory> sqlSessionFactoryList;
//    @Resource
//    private DataSourceUtil dateSourceUtil;
//
//
//    @PostConstruct
//    public void init() {
//        // 确保 TransInterceptor 被添加到 chain 的最后面
//        TransInterceptor transInterceptor = new TransInterceptor(dateSourceUtil);
//
//        for (SqlSessionFactory sqlSessionFactory : sqlSessionFactoryList) {
//            List<Interceptor> interceptors = sqlSessionFactory.getConfiguration().getInterceptors();
//            synchronized (this){
//                if (interceptors.size() <= 1 &&!containsInterceptor(sqlSessionFactory.getConfiguration(), transInterceptor)) {
//                    sqlSessionFactory.getConfiguration().addInterceptor(transInterceptor);
//                }
//            }
//        }
//    }
//
//    private boolean containsInterceptor(Configuration configuration, TransInterceptor interceptor) {
//        return configuration.getInterceptors().stream().anyMatch(config->interceptor.getClass().isAssignableFrom(config.getClass()));
//    }
//}
