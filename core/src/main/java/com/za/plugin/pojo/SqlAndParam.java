package com.za.plugin.pojo;

import org.apache.ibatis.mapping.ParameterMapping;

import java.util.List;

public class SqlAndParam {
    private String sql;
    private List<ParameterMapping> parameterMappings;

    public SqlAndParam(String sql, List<ParameterMapping> parameterMappings) {
        this.sql = sql;
        this.parameterMappings = parameterMappings;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public List<ParameterMapping> getParameterMappings() {
        return parameterMappings;
    }

    public void setParameterMappings(List<ParameterMapping> parameterMappings) {
        this.parameterMappings = parameterMappings;
    }
}
