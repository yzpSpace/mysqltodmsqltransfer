package com.za.plugin.pojo;

public class PredixStrAndCount {
    private String iteratorPrefix;
    private int count;

    public PredixStrAndCount(String iteratorPrefix, int count) {
        this.iteratorPrefix = iteratorPrefix;
        this.count = count;
    }

    public String getIteratorPrefix() {
        return iteratorPrefix;
    }

    public void setIteratorPrefix(String iteratorPrefix) {
        this.iteratorPrefix = iteratorPrefix;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
