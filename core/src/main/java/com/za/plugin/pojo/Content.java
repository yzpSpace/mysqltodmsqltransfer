package com.za.plugin.pojo;

public class Content {
    private String expectStr;
    private int end;

    public String getExpectStr() {
        return expectStr;
    }

    public int getEnd() {
        return end;
    }

    public Content(String expectStr, int end) {
        this.expectStr = expectStr;
        this.end = end;
    }
}
