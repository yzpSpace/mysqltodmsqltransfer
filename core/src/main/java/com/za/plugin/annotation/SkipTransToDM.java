package com.za.plugin.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 当该注解加在Mapper类，默认该Mapper类的所有的方法都会进入插件逻辑，如果该注解加在Mapper 的某一方法且excludeMethod为true，
 * 则该方法不进入到插件逻辑，如果该注解加在某个方法，则默认该方法进入插件逻辑
 */
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SkipTransToDM {
    boolean excludeMethod() default false;

}
