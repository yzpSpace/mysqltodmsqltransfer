package com.za.plugin.transfer.func;


// 很多函数 MySQL 和达梦都支持，但只是写法稍微有点不同，达梦不支持的 MySQL 函数一般也可以用其他函数进行替换
public interface FunctionTransfer {

    boolean isSupport(String sql);

    String transfer(String sql);
}
