package com.za.plugin.transfer.func;


import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.ast.expr.SQLMethodInvokeExpr;
import com.alibaba.druid.sql.dialect.mysql.parser.MySqlStatementParser;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlASTVisitorAdapter;

// 把 str_to_date() 替换为 date_format()
public class StrToDateFunctionTransfer implements FunctionTransfer {
    @Override
    public boolean isSupport(String sql) {
        return clearSpace(sql).contains("str_to_date(");
    }

    private String clearSpace(String sql) {
        return sql.toLowerCase().replaceAll("str_to_date\\s*\\(", "str_to_date(") ;
    }

    @Override
    public String transfer(String sql) {
        MySqlStatementParser parser = new MySqlStatementParser(sql);
        SQLStatement sqlStatement = parser.parseStatement();
        sqlStatement.accept(new MySqlASTVisitorAdapter() {
            @Override
            public boolean visit(SQLMethodInvokeExpr expr) {
                if (expr.getMethodName().equalsIgnoreCase("str_to_date")) {
                    expr.setMethodName("date_format");
                }
                return super.visit(expr);
            }
        });
        return SQLUtils.toMySqlString(sqlStatement);

    }
}


//        sql = clearSpace(sql);
//        sql = sql.replace("str_to_date(", "date_format(");
//        return sql;