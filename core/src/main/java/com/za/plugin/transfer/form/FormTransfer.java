package com.za.plugin.transfer.form;

// 做一些 SQL 语法和风格的替换，主要是 group by，having 不支持别名，在 case when ， join 中插入一些值让 SQL 语句能在
// 达梦中运行
public interface FormTransfer {
    boolean isSupport(String sql);

    String transfer(String sql);
}
