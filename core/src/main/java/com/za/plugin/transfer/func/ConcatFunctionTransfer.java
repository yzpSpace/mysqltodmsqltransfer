package com.za.plugin.transfer.func;

import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLExpr;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.ast.expr.SQLMethodInvokeExpr;
import com.alibaba.druid.sql.dialect.mysql.parser.MySqlStatementParser;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlASTVisitorAdapter;

import java.util.List;

/**
 * 解决 concat(data_sub(xxx),' 00:00:00') 的问题
 */
public class ConcatFunctionTransfer implements FunctionTransfer {

    @Override
    public boolean isSupport(String sql) {
        return sql.toLowerCase().contains("concat");
    }

    @Override
    public String transfer(String sql) {
        MySqlStatementParser parser = new MySqlStatementParser(sql);
        SQLStatement sqlStatement = parser.parseStatement();
        sqlStatement.accept(new MySqlASTVisitorAdapter() {
            @Override
            public boolean visit(SQLMethodInvokeExpr expr) {
                if (!expr.getMethodName().equalsIgnoreCase("concat")) {
                    return super.visit(expr);
                }
                List<SQLExpr> sqlExprs = expr.getParameters();
                if (sqlExprs.size() != 2) {
                    return super.visit(expr);
                }
                if (SQLUtils.toMySqlString(sqlExprs.get(0)).startsWith("date_") &&
                        SQLUtils.toMySqlString(sqlExprs.get(1)).contains(" 00:00:00")) {
                    expr.getParameters().set(1, SQLUtils.toSQLExpr("''"));
                }
                return super.visit(expr);
            }
        });
        return SQLUtils.toMySqlString(sqlStatement);
    }
}
