package com.za.plugin.transfer.form;

public class LikeFormTransfer implements FormTransfer {
    @Override
    public boolean isSupport(String sql) {
        return sql.contains(" like ") && sql.contains("%");
    }

    /**
     * JsqlParser 解析 '%'?'%' 失败，所以不能使用访问者模式去访问了
     *
     * @param sql
     * @return
     */
    @Override
    public String transfer(String sql) {
        sql = sql.replaceAll("like '%'\\?'%'", "like concat('%',?,'%')")
                .replaceAll("like '%'\\?", "like concat('%',?)")
                .replaceAll("like \\?'%'", "like concat(?,'%')");
        return sql;
    }
}
