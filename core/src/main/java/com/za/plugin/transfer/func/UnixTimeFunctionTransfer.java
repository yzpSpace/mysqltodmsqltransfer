package com.za.plugin.transfer.func;

// 把 from_unixtime(unix_timestamp('xxxx-xx-xx'),'%Y-%m-%d') 替换为 date_format()
public class UnixTimeFunctionTransfer implements FunctionTransfer {

    @Override
    public boolean isSupport(String sql) {
        String unixTimeStr = "from_unixtime(unix_timestamp";
        return sql.toLowerCase().contains(unixTimeStr);
    }


    @Override
    public String transfer(String sql) {
        String unixTimeStr = "from_unixtime(unix_timestamp";
        sql=sql.toLowerCase();
        while (sql.contains(unixTimeStr)) {
            int idx = sql.indexOf(unixTimeStr);
            int stripIdx = sql.indexOf(")", idx);
            sql = sql.substring(0, idx) + "date_format" + sql.substring(idx + unixTimeStr.length(), stripIdx) + sql.substring(stripIdx + 1);
        }
        return sql;
    }
}
