package com.za.plugin.transfer.form.insertupdate;

import org.apache.ibatis.mapping.ParameterMapping;

import java.util.List;
import java.util.Map;
import java.util.Set;

// 做 update ，insert ，replace into ，on duplicate key update 的转换
public interface StyleTransfer {

    boolean isSupport(String sql);

    String transfer(String sql, String tableName
            , List<String> autoIncrProperties , List<ParameterMapping> parameterMappingsCopy,
                    List<ParameterMapping> parameterMappings, Set<String> pKs, Map<String, List<String>> pkAndUniqueKeys);
}
