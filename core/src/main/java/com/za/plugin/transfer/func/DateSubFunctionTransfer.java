package com.za.plugin.transfer.func;


import com.za.plugin.pojo.Content;
import com.za.plugin.util.StrUtil;

// 修改  date_sub()
public class DateSubFunctionTransfer implements FunctionTransfer {
    @Override
    public boolean isSupport(String sql) {
        return clearSpace(sql).contains("date_sub(");
    }

    private String clearSpace(String sql) {
        return sql.toLowerCase().replaceAll("date_sub\\s*\\(", "date_sub(");
    }


    @Override
    public String transfer(String sql) {
        sql = clearSpace(sql);
        if (sql.contains("date_sub(")) {
            int idx = sql.indexOf("date_sub(");
            while (idx >= 0) {
                int left = sql.indexOf('(', idx);
                Content expectContent = StrUtil.getExpectContent(sql, left);
                assert expectContent != null;
                String expectStr = expectContent.getExpectStr();
                int end = expectContent.getEnd();
                String[] split = expectStr.split(",");
                String initTime = split[0];
                String[] other = split[1].trim().split("\\s+");
                String interval = other[0];
                String num = other[1];
                String timeUnit = other[2];
                String sb = "date_sub( " + initTime + "," + interval + " \'" + num + "\' " + timeUnit + " )";
                sql = sql.substring(0, idx) + sb + sql.substring(end);
                idx = sql.indexOf("date_sub(", idx + "date_sub(".length());
            }
        }
        return sql;
    }
}
