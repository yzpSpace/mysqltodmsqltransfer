package com.za.plugin.transfer.form;

public class EliminateKeywordAlias implements FormTransfer {



    @Override
    public boolean isSupport(String sql) {
        return true;
//        return SqlUtil.isSelect(sql);
    }

    /**
     * 仅对 select 的代码的表别名进行加 ""
     *
     * @param sql
     * @return
     */
    @Override
    public String transfer(String sql) {
        if (sql.contains("group.")) {
            sql = sql.replaceAll("\\s+", " ");
            sql = sql
                    // .replaceAll(" group ", " \"group\" ")
                    // .replaceAll(" group\\.", " \"group\".")
                    .replaceAll(",group\\.", ",\"group\".")
                    .replaceAll("=group\\.", "=\"group\".")
                    .replaceAll("\\)group\\.", ")\"group\".")
                    // .replaceAll(" \"group\" by ", " group by ")
                    .replaceAll("(,|\\.|=)group(\\s*|\\.|,)", "$1\"group\"$2");
        }
        //  a.desc "desc"
        // if (sql.contains("desc")) {
//            sql = sql.replaceAll("\\s+desc\\s+"," \"desc\" ");


            // sql = sql.replaceAll("\\.desc ", ".\"desc\" ")
            //         .replaceAll(",\\s*desc,", ",\"desc\",")
            //         .replaceAll(",\\s*desc\\s*\\)", ",\"desc\")")
            //         .replaceAll("(,|.)desc(\\s*|.|,)", "$1\"desc\"$2");
//            Pattern compile = Pattern.compile("\\s+order\\s+by\\s+(a-z|A-Z|.|0-9)+\\s+\"desc\"");

//            while (sql.contains("\"desc\"")){
//                int idx = sql.indexOf("\"desc\"");
//                Matcher matcher = compile.matcher(sql);
//                if (matcher.find()){
//
//                }
//            }
//         }
        //  a.identity "identity"
        if (sql.contains("identity")) {
            sql = sql.replaceAll("\\.identity ", ".\"IDENTITY\" ")
                    .replaceAll(",\\s*identity,", ",\"IDENTITY\",")
                    .replaceAll(",\\s*identity\\s*\\)", ",\"IDENTITY\")")
                    .replaceAll("\\s+identity\\s+", " \"IDENTITY\" ");
                    // .replaceAll("(,|\\s+|\\.)identity(\\s*|\\.|,)", "$1\"IDENTITY\"$2");
        }
        if (sql.contains("user")) {
            sql = sql.replaceAll("\\.user ", ".\"USER\" ")
                    .replaceAll(",\\s*user,", ",\"USER\",")
                    .replaceAll(",\\s*user\\s*\\)", ",\"USER\")")
                    .replaceAll("\\s+user\\s+", " \"USER\" ");
                    // .replaceAll("(,|\\s+|\\.)user(\\s*|\\.|,)", "$1\"user\"$2");
        }
        if (sql.contains("comment")) {
            sql = sql.replaceAll("\\.comment ", ".\"comment\" ")
                    .replaceAll(",\\s*comment,", ",\"comment\",")
                    .replaceAll(",\\s*comment\\s*\\)", ",\"comment\")")
                    .replaceAll("\\s+comment\\s+", " \"comment\" ")
                    .replaceAll("'\\s*comment\\s*'","\"comment\"")
                    .replaceAll("(,|\\s+|\\.)comment(\\s*|\\.|,)", "$1\"comment\"$2");
        }
        if (sql.contains("ref")) {
            sql = sql.replaceAll("\\.ref ", ".\"ref\" ")
                    .replaceAll(",\\s*ref,", ",\"ref\",")
                    .replaceAll(",\\s*ref\\s*\\)", ",\"ref\")")
                    .replaceAll("\\s+ref\\s+", " \"ref\" ");
                    // .replaceAll("(,|\\s+|\\.)ref(\\s*|\\.|,)", "$1\"ref\"$2");
        }
        if (sql.contains("percent")) {
            sql = sql.replaceAll("\\.percent ", ".\"percent\" ")
                    .replaceAll(",\\s*percent,", ",\"percent\",")
                    .replaceAll(",\\s*percent\\s*\\)", ",\"percent\")")
                    .replaceAll("\\s+percent\\s+", " \"percent\" ");
                    // .replaceAll("(,|\\s+|\\.)percent(\\s*|\\.|,)", "$1\"percent\"$2");
        }
        if (sql.contains("context")) {
            sql = sql.replaceAll("\\.context ", ".\"context\" ")
                    .replaceAll(",\\s*context,", ",\"context\",")
                    .replaceAll(",\\s*context\\s*\\)", ",\"context\")")
                    .replaceAll("\\s+context\\s+", " \"context\" ")
                    .replaceAll("('|`)context('|`)"," \"context\" ")
                     .replaceAll("(,|\\s+|\\.)context(\\s*|\\.|,)", "$1\"context\"$2");
        }
        return sql;
    }
}
