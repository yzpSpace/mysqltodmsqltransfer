package com.za.plugin.transfer.form;

public class TruncateFormTransfer implements FormTransfer {
    @Override
    public boolean isSupport(String sql) {
        return sql.trim().startsWith("truncate ");
    }

    /**
     * 只考虑删除一张表
     * @param sql
     * @return
     */
    @Override
    public String transfer(String sql) {
        sql = sql.trim();
        String tblName = sql.split(" ")[1];
        if (tblName.equals("table")){
            return "truncate table " +sql.split(" ")[2];
        }
        return "truncate table " + tblName;
//        int truncateIdx = sql.indexOf("truncate ");
//        StringBuilder sb = new StringBuilder();
//        int left=0;
//        while (truncateIdx>=0){
//            sb.append(sql,left,truncateIdx+9).append()
//        }
//        return;
    }
}
