package com.za.plugin.transfer.form;//package com.za.plugin.transfer.form;
//
//
//
//import com.za.plugin.util.SqlUtil;
//import com.za.plugin.util.StrUtil;
//
//import java.util.Map;
//import java.util.stream.Stream;
//
////  达梦不支持 having 后面使用别名。
////  把 having 后的别名替换为原来的样子, having 可能是在嵌套子 select 查询里。
//
//public class HavingFormTransfer implements FormTransfer {
//    @Override
//    public boolean isSupport(String sql) {
//        return sql.contains("having ");
//    }
//
//    @Override
//    public String transfer(String sql) {
//        if (sql.contains("having ")) {
//            int havingIdx = sql.indexOf("having ");
//            while (havingIdx >= 0) {
//                int idx = sql.indexOf("select ");
//                while (idx >= 0) {
//                    if (!checkValid(sql, idx, havingIdx)) {
//                        idx = sql.indexOf("select ", idx + "select ".length());
//                        continue;
//                    }
//                    Map<String, String> selectProperties = SqlUtil.getSelectProperties(sql, idx);
//                    int end = Stream.of(sql.indexOf("limit ", havingIdx), sql.indexOf("order by ", havingIdx), getEndChar(sql, havingIdx)
//                    ).filter(it -> it != -1).min(Integer::compareTo).get();
//                    String substring = sql.substring(havingIdx + "having ".length(), end).toLowerCase();
//                    for (Map.Entry<String, String> entry : selectProperties.entrySet()) {
//                        String key = entry.getKey().toLowerCase();
//                        if (substring.contains(key)) {
//                            String value = entry.getValue().toLowerCase();
//                            // 在实际测试中发现，如果 别名对应的实际值有 "?" , 就不需要进行替换
//                            // having count(*) 且 select count(*) count 也不需要替换
//                            if (value.contains("?")||(substring.replaceAll("\\s+","").contains(value.replaceAll("\\s+","")))) {
//                                continue;
//                            }
//                            substring = StrUtil.swapWord(substring, key, value);
//                        }
//                    }
//                    StringBuilder sb = new StringBuilder();
//                    sb.append(sql, 0, havingIdx + 7).append(substring);
//                    if (end < sql.length()) {
//                        sb.append(sql.substring(end));
//                    }
//                    sql = sb.toString();
//                    break;
//                }
//                havingIdx = sql.indexOf("having ", havingIdx + 7);
//            }
//        }
//        return sql;
//    }
//
//    private static boolean checkValid(String sql, int idx, int havingIdx) {
//        int j = idx, leftNum = 0;
//        while (j < havingIdx) {
//            if (sql.charAt(j) == '(') {
//                leftNum++;
//            } else if (sql.charAt(j) == ')') {
//                leftNum--;
//            }
//            j++;
//        }
//        return leftNum == 0;
//    }
//    private static int getEndChar(String sql, int havingIdx) {
//        int j = havingIdx, rightNum = 0, len = sql.length();
//        while (j < len) {
//            if (sql.charAt(j) == ')') {
//                rightNum++;
//            } else if (sql.charAt(j) == '(') {
//                rightNum--;
//            }
//            if (rightNum == 1) {
//                return j;
//            }
//            j++;
//        }
//        return len;
//    }
//}
