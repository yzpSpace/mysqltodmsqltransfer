package com.za.plugin.transfer.func;

// 达梦恰好也有这个
// 把 substring_index() 替换为 substr()
//public class SubstringIndexFunctionTransfer implements FunctionTransfer {
//
//    @Override
//    public boolean isSupport(String sql) {
//        return clearSpace(sql).contains("substring_index");
//    }
//
//    private String clearSpace(String sql) {
//        return sql.replaceAll("substring_index\\s*\\(", "substring_index(");
//    }
//
//    @Override
//    public String transfer(String sql) {
//        sql = clearSpace(sql);
//        if (sql.contains("substring_index")) {
//            int idx = sql.indexOf("substring_index");
//            while (idx >= 0) {
//                int left = sql.indexOf('(', idx);
//                Content contentObj = StrUtil.getExpectContent(sql, left);
//                String content = contentObj.getExpectStr();
//
//                // 可能有 "wm_concat(report.report_operator) ,',',1" 这种情况
//                int end = contentObj.getEnd();
//                int idxFirst = content.indexOf(",");
//                int idxLast = content.lastIndexOf(",");
//                String firstEle = content.substring(0, idxFirst);
//                String secondEle = content.substring(idxFirst + 1, idxLast);
//                String thirdEle = content.substring(idxLast + 1);
//
////                String[] split = content.split(",");
//                String str = firstEle.substring(1, firstEle.length() - 1);
//                String delimiter = secondEle.charAt(1) + "";
//                boolean isAdd = false;
//                String escapeStr = getEscapeChar(delimiter);
//                if (!escapeStr.equals(delimiter)) {
//                    delimiter = escapeStr;
//                    isAdd = true;
//                }
//                int num = Integer.parseInt(thirdEle.trim());
//                StringBuilder sb = new StringBuilder();
//                String[] splitStr = str.split(delimiter);
//                if (num > 0) {
//                    for (int i = 0; i < num; i++) {
//                        addStr(delimiter, isAdd, sb, splitStr[i]);
//                    }
//                } else if (num < 0) {
//                    num = Math.abs(num);
//                    for (int i = splitStr.length - num; i < splitStr.length; i++) {
//                        addStr(delimiter, isAdd, sb, splitStr[i]);
//                    }
//                }
//                if (num != 0) {
//                    sb.deleteCharAt(sb.length() - 1);
//                }
//                int index = str.indexOf(sb.toString());
//                StringBuilder ret = new StringBuilder();
//                ret.append(sql, 0, idx).append("substr('").append(str).append("'").append(",").append(index).append(",").append(sb.length()).append(")").append(sql.substring(end));
//                System.out.println(ret);
//                sql = ret.toString();
//                idx = sql.indexOf("substring_index");
//            }
//        }
//        return sql;
//    }
//
//    private static String getEscapeChar(String delimiter) {
//        // 对  ，. ; 进行转义。 []{}()本来也需要转义，但因为它们一般不作为分隔符，不必要进行转义
//        Set<Character> set = new HashSet<>(Arrays.asList(',', ';', '.'));
//        char ch = delimiter.charAt(0);
//        if (!set.contains(ch)) {
//            return delimiter;
//        }
//        return "\\" + ch;
//    }
//
//    private void addStr(String delimiter, boolean isAdd, StringBuilder sb, String splitStr) {
//        sb.append(splitStr);
//        if (isAdd) {
//            sb.append(delimiter.charAt(1));
//        } else {
//            sb.append(delimiter);
//        }
//    }
//}
