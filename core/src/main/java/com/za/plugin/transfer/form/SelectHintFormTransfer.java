package com.za.plugin.transfer.form;

// https://blog.csdn.net/qq_37898570/article/details/126951104
// 添加 GROUP_OPT_FLAG(1) hint。 hint 放在最后，影响小一点
public class SelectHintFormTransfer implements FormTransfer {
    @Override
    public boolean isSupport(String sql) {
        sql = sql.trim();
        return sql.startsWith("select ");
    }


    @Override
    public String transfer(String sql) {
        if (sql.startsWith("select ")) {
            int index = sql.indexOf("select");
            if (index >= 0) {
                sql = sql.substring(0, index) + " select " + " /*+ GROUP_OPT_FLAG(1)*/ "
                        + sql.substring(index + "select".length());
            }
        }
        return sql;
    }
}
