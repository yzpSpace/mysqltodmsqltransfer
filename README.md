 

###  介绍

一个 mybatis 插件，可以把 MySQL 的 sql 转为合法的达梦的 sql，主要用于信创达梦数据库迁移

###  软件架构 

一个 mybatis 插件，在 insert，update 时跳过插入或修改自增键，把 replace into，on duplicate key update 改为 
达梦支持的 merge into 语法，把 select 的 ` 统一修改为 "，其他已知的写法和用法转换。
一般情况下如果要自测 sql 是否已经替换好，可以在 SQLHelper#main 方法中替换一下要处理的 sql ，把从控制台打印出来的已转换好的 sql 语句拿到
达梦数据库中去执行，看结果是否正确。 
 
###  安装教程

拷贝本项目代码到用户项目中的一个maven模块即可使用，以后所有的 sql 都会被本插件拦截，修改并执行。
如果希望某个接口或方法跳过本插件，在接口或方法加上 @SkipTransToDM(excludeMethod=true) 即可。

### 使用说明

 

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
